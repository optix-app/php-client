<?php

namespace Optix\Helpers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class WebhookValidator
{
    /**
     * @throws Exception
     */
    public static function validateSignature(Request $request, ?string $secret = null): bool
    {
        if (empty($secret)) {
            $secret = Config::get('optix.webhook-secret');
        }

        if (empty($secret)) {
            throw new Exception('Invalid Optix webhook secret');
        }

        $clientId = $request->get('client_id', '');
        $createdAt = $request->get('created_timestamp', '');

        $validSignature = self::calculateSignature($clientId, $secret, $createdAt);

        return $validSignature === $request->get('request_signature');
    }

    public static function calculateSignature(string $clientId, string $secret, string $createdAt): string
    {
        return sha1($clientId.$secret.$createdAt);
    }

    /**
     * @throws Exception
     */
    public static function validate(Request $request)
    {
        if (!self::validateSignature($request)) {
            throw new Exception('Invalid Optix webhook signature');
        }
    }
}

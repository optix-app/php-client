<?php

declare(strict_types=1);

namespace Optix;

use Illuminate\Support\ServiceProvider;

class OptixServiceProvider extends ServiceProvider
{
    public const CONFIG_PATH = __DIR__.'/../config/optix.php';

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                self::CONFIG_PATH => config_path('optix.php'),
            ], 'config');
            $this->publishes([
                __DIR__.'/../database/migrations' => database_path('migrations'),
            ], 'optix-migrations');
        }

        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(self::CONFIG_PATH, 'optix');
        app('router')->aliasMiddleware('optix.token', \Optix\Http\Middleware\EnsureOptixToken::class);
    }
}

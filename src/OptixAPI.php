<?php

declare(strict_types=1);

namespace Optix;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Middleware;

class OptixAPI
{
    /** @var Client */
    protected $guzzleClient;

    private static $queryFileCache = [];

    /**
     * Build a new Optix API Client
     *
     * @param string $token
     * @param array $config Guzzle additional parameters
     */
    public function __construct(protected string $token, array $config = [])
    {
        $this->guzzleClient = $this->initGuzzleClient($config);
    }

    /**
     * @param array $config
     * @return Client
     */
    private function initGuzzleClient(array $config): Client
    {
        $defaults = [
            'handler'  => $this->guzzleHandlers(),
            'base_uri' => Config::get('optix.api_url'),
            'headers'  => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
                'User-Agent'   => 'PHP Optix Client'
            ],
        ];

        return new Client($config + $defaults);
    }


    /**
     * @return HandlerStack
     */
    private function guzzleHandlers(): HandlerStack
    {
        $middlewareStack = new HandlerStack();
        $middlewareStack->setHandler(new CurlHandler());

        // Adds Optix API token
        $middlewareStack->push(function (callable $handler) {
            return function (RequestInterface $request, array $options) use ($handler) {
                if ($this->token) {
                    $request = $request->withHeader('access_token', $this->token);
                }

                return $handler($request, $options);
            };
        });

        $middlewareStack->push(
            Middleware::mapResponse(function (ResponseInterface $response) {
                $response = new GraphQLResponse(
                    $response->getStatusCode(),
                    $response->getHeaders(),
                    $response->getBody(),
                    $response->getProtocolVersion(),
                    $response->getReasonPhrase()
                );
                $this->lastResponse = $response;

                return $response;
            })
        );

        // Audit handlers
        if (Config::get('optix.audit')) {
            $middlewareStack->push(function (callable $handler) {
                return function (RequestInterface $request, array $options) use ($handler) {
                    $microtime = microtime(true);
                    Log::debug('[Audit] OptixAPI request: ', [
                        'id' => $microtime,
                        'body' => $request->getBody(),
                        'headers' => $request->getHeaders()
                    ]);
                    $response = $handler($request, $options);
                    $response->then(
                        function ($response) use ($microtime) {
                            Log::debug('[Audit] OptixAPI success: ', [
                                'id' => $microtime,
                                'code' => $response->getStatusCode(),
                                'headers' => $response->getHeaders(),
                                'body' => $response->getBody(),
                                'protocol' => $response->getProtocolVersion(),
                                'reason' => $response->getReasonPhrase()
                            ]);
                        },
                        function ($response) use ($microtime) {
                            Log::debug('[Audit] OptixAPI failure: ', [
                                'id' => $microtime,
                                'code' => $response->getStatusCode(),
                                'headers' => $response->getHeaders(),
                                'body' => $response->getBody(),
                                'protocol' => $response->getProtocolVersion(),
                                'reason' => $response->getReasonPhrase()
                            ]);
                        }
                    );
                    return $response;
                };
            });
        }

        return $middlewareStack;
    }


    /**
     * Query GraphQL using a file query (considering base_path)
     * @param string $graphql_query_file_path
     * @param array $variables query variables
     * @return GraphQLResponse
     */
    public function query(string $graphql_query_file_path, array $variables = []): GraphQLResponse
    {
        $file_path = base_path($graphql_query_file_path);
        return $this->queryFromFile($file_path, $variables);
    }

    /**
    * Query GraphQL using a file query (raw path)
    * @param string $graphql_query_file_path
    * @param array $variables query variables
    * @return GraphQLResponse
    */
    public function queryFromFile(string $graphql_query_file_path, array $variables = []): GraphQLResponse
    {
        $file_path = realpath($graphql_query_file_path);
        if ($file_path === false) {
            throw new \Exception("Invalid file path '$graphql_query_file_path'");
        }
        if (isset(static::$queryFileCache[$file_path])) {
            $query = static::$queryFileCache[$file_path];
        } else {
            $query = trim(file_get_contents($file_path));
            if (Config::get('optix.query-file-cache')) {
                static::$queryFileCache[$file_path] = $query;
            }
        }
        return $this->queryFromString($query, $variables);
    }

    /**
     * Query GraphQL using a string query
     *
     * @param string $graphql_query_string GraphQL query
     * @param array $variables query variables
     * @return GraphQLResponse
     */
    public function queryFromString(string $graphql_query_string, array $variables = []): GraphQLResponse
    {
        if (!strlen(trim($graphql_query_string))) {
            throw new \Exception("Empty GraphQL query");
        }
        $response = $this->guzzleClient->post('', [
            RequestOptions::JSON => [
                'query'     => $graphql_query_string,
                'variables' => $variables,
            ],
        ]);

        return $response;
    }

    /**
     * Remove all GraphQL files cached
     *
     * @return void
     */
    public function clearFileCache(): void
    {
        static::$queryFileCache = [];
    }
}

<?php

namespace Optix\Events;

class AppInstalled
{
    public function __construct(public string $optix_organization_id)
    {
    }
}

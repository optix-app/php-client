<?php

namespace Optix\Events;

class GenericWebhook
{
    public function __construct(
        public string $event,
        public string $optix_organization_id,
        public array $payload
    ) {
    }
}

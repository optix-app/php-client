<?php

namespace Optix\Events;

class AppUninstalled
{
    public function __construct(public string $optix_organization_id)
    {
    }
}

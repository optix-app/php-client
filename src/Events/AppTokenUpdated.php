<?php

namespace Optix\Events;

class AppTokenUpdated
{
    public function __construct(public string $optix_organization_id)
    {
    }
}

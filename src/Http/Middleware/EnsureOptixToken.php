<?php

namespace Optix\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Optix\OptixAPI;

class EnsureOptixToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  ?string  $role
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ?string $role = null)
    {
        $token = $request->bearerToken() ?? $request->input('access_token') ?? $request->input('token');

        $this->validateToken($token);

        $cache_ttl = Config::get('optix.middleware.token-cache-ttl');

        $should_cache = $cache_ttl && $cache_ttl > 0;

        $cached_data = $should_cache ? Cache::get("middleware_tkn_{$token}") : null;

        if (!$cached_data) {
            $optixService = new OptixAPI($token);
            $token_info = $optixService->queryFromFile(
                Config::get('optix.middleware.custom-query-path') ?? __DIR__.'/../../../graphql-queries/optix/EnsureOptixToken.graphql'
            );

            if ($token_info->hasErrors()) {
                throw new \Exception('Invalid token');
            }

            $data = $token_info->getData('me');

            if ($should_cache) {
                Cache::put("middleware_tkn_{$token}", $data, $cache_ttl);
            }
        } else {
            $data = $cached_data;
        }

        $this->checkRole($data, $role);

        $request = $request->merge([
            'token_info' => $data,
        ]);

        return $next($request);
    }

    /**
     * Check if data contains $role properties
     * @param array $data
     * @param null|string $role
     * @return void
     * @throws Exception
     */
    private function checkRole(array $data, ?string $role): void
    {
        if ($role === null) {
            return;
        }

        if ($role !== 'admin' && $role !== 'active_user') {
            throw new \Exception("Invalid user type at middleware configuration");
        }

        if (!isset($data['organization'])) {
            throw new \Exception("Invalid token, query or organization. Your base query should contain a me query with organization, user->is_admin and user->is_active");
        }

        if ($role === 'admin') {
            if (!$data['user']['is_admin']) {
                throw new \Exception("Token without admin permissions");
            }
        }

        if ($role === 'active_user') {
            if (!$data['user']['is_active'] && !$data['user']['is_admin']) {
                throw new \Exception("Token without user permissions");
            }
        }
    }

    /**
     * Validate token
     *
     * @param string|null $token
     * @return void
     */
    private function validateToken(?string $token)
    {
        if (!$token || !is_string($token)) {
            throw new \Exception('Missing access_token/token parameter');
        }

        $suffix = substr($token, -1);

        if (strlen($token) != 41 || ($suffix !== 'p' && $suffix !== 'o')) {
            throw new \Exception('Only personal and organization Optix tokens supported '.$token);
        }
    }
}

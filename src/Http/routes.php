<?php

use Illuminate\Support\Facades\Route;
use Optix\Http\Controllers\WebhookGenericHandler;
use Optix\Http\Controllers\WebhookInstall;
use Optix\Http\Controllers\WebhookTokenUpdated;
use Optix\Http\Controllers\WebhookUninstall;

Route::post('/optix-webhook/app_install', WebhookInstall::class);
Route::post('/optix-webhook/app_uninstall', WebhookUninstall::class);
Route::post('/optix-webhook/organization_token_updated', WebhookTokenUpdated::class);
Route::post('/optix-webhook/listener', WebhookGenericHandler::class);

<?php

namespace Optix\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Optix\Events\AppTokenUpdated;
use Optix\Helpers\WebhookValidator;

class WebhookTokenUpdated extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        /*
        Sample:
        {
            "organization_token": "5c26d23356…3960870bfo",
            "client_id": "ce81e1fbf3d7…ba561d5d",
            "created_timestamp": 1545092451,
            "organization_id": 3568,
            "event": "organization_token_updated",
            "request_signature": "1207e01d133aa…4329c48ff0f"
        }
        */
        WebhookValidator::validate($request);

        $current_organization = DB::table('optix_organizations')
            ->where('optix_organization_id', $request->get('organization_id'))
            ->first();

        if ($current_organization) {
            DB::table('optix_organizations')
                ->where('optix_organization_id', $request->get('organization_id'))
                ->update([
                    'updated_at' => Carbon::now(),
                    'token' => $request->get('organization_token'),
                ]);
        }

        event(new AppTokenUpdated($request->get('organization_id')));
    }
}

<?php

namespace Optix\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Optix\Events\GenericWebhook;
use Optix\Helpers\WebhookValidator;

class WebhookGenericHandler extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        /*
        Sample:
        {
            "organization_token": "5c26d23356…3960870bfo",
            "client_id": "ce81e1fbf3d7…ba561d5d",
            "created_timestamp": 1545092451,
            "organization_id": 3568,
            "event": "app_install",
            "request_signature": "1207e01d133aa…4329c48ff0f"
        }
        */
        WebhookValidator::validate($request);

        event(new GenericWebhook(
            $request->get('event'),
            $request->get('organization_id'),
            $request->input(),
        ));
    }
}

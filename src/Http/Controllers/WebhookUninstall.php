<?php

namespace Optix\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Optix\Events\AppUninstalled;
use Optix\Helpers\WebhookValidator;

class WebhookUninstall extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        /*
        Sample:
        {
            "client_id": "ce81e1fbf3d73…0ba561d5d",
            "created_timestamp": 1545092451,
            "organization_id": 3568,
            "event": "app_uninstall",
            "request_signature": "1207e01d133aa…a4329c48ff0f"
        }
        */
        WebhookValidator::validate($request);

        $current_organization = DB::table('optix_organizations')
            ->where('optix_organization_id', $request->get('organization_id'))
            ->first();

        if ($current_organization) {
            DB::table('optix_organizations')
                ->where('optix_organization_id', $request->get('organization_id'))
                ->update([
                    'deleted_at' => Carbon::now(),
                ]);
        }

        event(new AppUninstalled($request->get('organization_id')));
    }
}

<?php

namespace Optix\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Optix\Events\AppInstalled;
use Optix\OptixAPI;
use Optix\Helpers\WebhookValidator;

class WebhookInstall extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        /*
        Sample:
        {
            "organization_token": "5c26d23356…3960870bfo",
            "client_id": "ce81e1fbf3d7…ba561d5d",
            "created_timestamp": 1545092451,
            "organization_id": 3568,
            "event": "app_install",
            "request_signature": "1207e01d133aa…4329c48ff0f"
        }
        */
        WebhookValidator::validate($request);

        $current_organization = DB::table('optix_organizations')
            ->where('optix_organization_id', $request->get('organization_id'))
            ->first();

        $optix_api = new OptixAPI($request->get('organization_token'));

        $organization_data = $optix_api->queryFromFile(__DIR__."/../../../graphql-queries/optix/FetchBasicOrganizationData.graphql");

        if ($organization_data->hasErrors()) {
            throw new \Exception("Error fetching data from Optix API at Webhook installation");
        }

        if ($current_organization) {
            DB::table('optix_organizations')
                ->where('optix_organization_id', $request->get('organization_id'))
                ->update([
                    'deleted_at' => null,
                    'name' => $organization_data->getData('me.organization.name'),
                    'timezone' => $organization_data->getData('me.organization.timezone'),
                    'logo' => $organization_data->getData('me.organization.logo'),
                    'token' => $request->get('organization_token'),
                    'updated_at' => Carbon::now(),
                ]);
        } else {
            DB::table('optix_organizations')
                ->insert([
                    'optix_organization_id' => $request->get('organization_id'),
                    'name' => $organization_data->getData('me.organization.name'),
                    'timezone' => $organization_data->getData('me.organization.timezone'),
                    'logo' => $organization_data->getData('me.organization.logo'),
                    'token' => $request->get('organization_token'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
        }

        event(new AppInstalled($request->get('organization_id')));
    }
}

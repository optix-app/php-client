<?php

declare(strict_types=1);

namespace Optix;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;

class GraphQLResponse extends Response
{
    /**
     * @const string
     */
    const RETRY_AFTER_HEADER = 'retry-after';

    /**
     * @const string
     */
    const RATE_LIMIT_REMAINING = 'X-RateLimit-Remaining';

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->json('errors') !== null;
    }

    /**
     * @return array
     */
    public function getErrors(): ?array
    {
        return $this->json('errors');
    }

    /**
     * @param string $path
     * @return mixed|null
     */
    public function getData(string $path = null)
    {
        if ($path === null) {
            return $this->json('data');
        }

        return $this->json("data.${path}");
    }

    /**
     * @param string|null $path
     */
    public function json(string $path = null)
    {
        $response = json_decode((string) $this->getBody(), true);

        if ($path === null) {
            return $response;
        }

        return Arr::get($response, $path) ?? null;
    }

    /**
     * @return int|null
     */
    public function getRetryAfter()
    {
        return Arr::first($this->getHeader(self::RETRY_AFTER_HEADER), function ($value) {
            return (int) $value;
        }, 0);
    }

    /**
     * @return int|null
     */
    public function getRemainingAvailableRequests()
    {
        return Arr::first($this->getHeader(self::RATE_LIMIT_REMAINING), function ($value) {
            return (int) $value;
        }, 0);
    }
}

<?php

// @formatter:off

/**
 * A helper to avoid errors by the syntax analyser
 *
 * This file should not be included in your code, only analyzed by your IDE!
 */

function config_path($path = ''): string
{
    return '';
}
function database_path($path = ''): string
{
    return '';
}
function app($string = ''): mixed
{
}
function base_path($path = ''): string
{
    return '';
}
function event(mixed $event)
{
}

throw new \Exception("This file should not be included!");

![alt text](https://www.optixapp.com/assets/common/optix-logo.svg "Optix")

# Optix PHP Library

This library enables your Laravel application to easily make requests to Optix GraphQL API.

> This library is designed to use PHP 8.1 and Laravel 9

## Installation

```bash
composer require optix-app/php-client
```

Use composer to install in your existing project. If you are starting a project from scratch, we recommend you to use our [Laravel boilerplate](https://gitlab.com/optix-app/optix-app-boilerplate).

Don't forget to publish the new configuration file.

```bash
php artisan vendor:publish --provider="Optix\OptixServiceProvider"
```

## Usage

### Using the `OptixAPI` to query Optix GraphQL API

Create the file `me.graphql` within a new project root folder called `graphql-queries`. Separating queries from your code avoid a cluttered code, and most modern code editors allow `.graphql` formatting.

```graphql
# graphql-queries/me.graphql
query me {
  me {
    user {
      name
      user_id
    }
  }
}
```

Then at the PHP...

```php
// Anywhere in your code
$optix_api = new \Optix\OptixAPI('<Your Optix GraphQL API Key>');
$response = $optix_api->query('graphql-queries/me.graphql');
print_r($response->getData());
```

If your query has variables you can declare them at the `.graphql` file and provide the variables at

```php
/*
# at your file graphql-queries/users.graphql
query users ($limit: Int) {
  users(limit: $limit){
    total
    data {
      user_id
      name
    }
  }
}
*/

// Anywhere
$optix_api = new \Optix\OptixAPI('<Your Optix GraphQL API Key>');
$response = $optix_api->query('graphql-queries/users.graphql', [
    'limit' => 5
]);
// Get entire response
print_r($response->getData());

// Get specific part of the response
print_r($response->getData('users.data'));

// Returns true if an "errors" key is found in the response
print_r($response->hasErrors());

// Get query errors
print_r($response->getErrors());
```

### Using the `EnsureOptixToken` to protect endpoints

At your route, use `optix.token` middleware, it will check the `token` and will add `token_info` to the request.

```php
// You can provide a role as parameter to protect the endpoint `admin` or `active_user` role
Route::middleware('optix.token:admin')->get('/test', function (Request $request) {
    // You can read at $request->input('token_info')
    /*
    {
        "organization": {
            "organization_id": "0000",
            "name": "Organization name",
            "timezone": "America/Vancouver",
            "logo": "https://url.to.image..."
        },
        "user": {
            "user_id": "0000",
            "fullname": "User name",
            "email": "user@example.com",
            "is_admin": true,
            "is_active": true,
            "image": {
                "has_image": false,
                "large": "https://url.to.image"
            }
        }
    }
    */

    // Return everything for this example
    return $request->input();
});
```

### Handling basic webhooks

This library also provides a basic handling for the most common webhooks related to platform apps:

- App installation (`app_install`)
- App uninstall (`app_uninstall`)
- Organization token updated (`organization_token_updated`)

You can check more details about these webhooks at [Optix Webhooks List](https://developer.optixapp.com/using-webhooks/webhooks-list/).

In order to save at the database the organizations that have installed the app, the service provider will register a few routes and create a migration file to create a table called `optix_organizations`. The service provider do not create models for the table, feel free to create them as you wish.

Don't forget to set at the `.env` file the `OPTIX_SECRET=` parameter, with the signature secret generated at your Optix Web Dashboard > App > Develop page. At the same page you should save a basic [settings file](https://developer.optixapp.com/app-settings-file/):

```json
{
  "webhooks": [
    {
      "event": "app_install",
      "url": "https://example.com/optix-webhook/app_install"
    },
    {
      "event": "app_uninstall",
      "url": "https://example.com/optix-webhook/app_uninstall"
    },
    {
      "event": "organization_token_updated",
      "url": "https://example.com/optix-webhook/organization_token_updated"
    }
  ]
}
```

> You can use a tool like `ngrok` or `expose` to make your local environment accessible to the webhooks

Its also possible to listen for events related to the app installation, so you can handle additional actions directly into your app:

```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;
use Optix\Events\AppInstalled;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen(function (AppInstalled $event) {
            // Handle $event->optix_organization_id ...
            // you can fetch additional data or trigger internal actions in your app
        });
    }
}
```

For other Optix webhooks you can also use a generic listener url:

```json
{
  "webhooks": [
    {
      "event": "assignment_started",
      "url": "https://example.com/optix-webhook/listener"
    },
    {
      "event": "invoice_paid",
      "url": "https://example.com/optix-webhook/listener"
    }
  ]
}
```

For the `/listener` url, all webhooks will trigger a generic webhook event:

```php
use Optix\Events\GenericWebhook;

Event::listen(function (GenericWebhook $event) {
    // Handle $event->event ...
    // Handle $event->optix_organization_id ...
    // Handle $event->payload ...
});
```

> The generic listener will only validate the signature and dispatch the event, no other action will be executed by the library (persist at the database or check key validity).